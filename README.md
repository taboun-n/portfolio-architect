# Architecture

## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)
* [Inspiration](#inspiration)
* [Contact](#contact)

## General info
This is a simple integration of a template created by myself with Adobe XD.

## Technologies
Project is created with:
* Node JS: 12.18.3
* npm: 6.14.7
* EJS: 3.1.5
* Express: 4.17.1
* Tailwind CSS: 1.7.3

Requires:
* npx

## Setup
To run this project, install it locally using npm:
```
$ cd portfolio-architect/
$ cd app/
$ npm install
$ npm run build
$ npm start
```

## Inspiration
All pictures are from [https://unsplash.com](https://unsplash.com), special credits to:
* [Viktor Jakovlev](https://unsplash.com/@apviktor)
* [Joakim Nadell](https://unsplash.com/@joakimnadell)
* [Riccardo Oliva](https://unsplash.com/@riccardo__oliva)
* [Jimmy Chang](https://unsplash.com/@photohunter)
* [Marc Olivier Jodoin](https://unsplash.com/@marcojodoin)

## Contact
Created by [Nicolas Tabountchikoff](https://taboun-n.gitlab.io) - feel free to contact me!
