const express = require('express');
const fs = require('fs');

const PORT = 8080;
const public_path = __dirname + '/public/';
const views_path = __dirname + '/views/';
const portfolio_path = public_path + 'portfolio/';

const app = express();
app.set('views', views_path);
app.set('view engine', 'ejs');
app.use('/static', express.static(public_path));

app.get('/', function (req, res) {
  const files = fs.readdirSync(portfolio_path)

  res.render('pages/home', { portfolio_pictures: files });
});

app.listen(PORT, () => {
  console.log(`Server app started on port ${PORT}`);
});
