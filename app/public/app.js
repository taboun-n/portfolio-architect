function MainMenu() {
  const $nav = document.getElementById('main-menu');
  const $input = $nav.querySelector('input');
  const $links = $nav.querySelectorAll('label a');

  $links.forEach($link => {
    $link.addEventListener('click', () => {
      $input.checked = false;
    });
  });
}

function CreateInputRadio(index, selected=false, onChange) {
  const $input = document.createElement('input');
  $input.setAttribute('type', 'radio');
  $input.setAttribute('name', 'carousel');
  $input.setAttribute('value', index.toString());
  $input.checked = selected;

  $input.addEventListener('change', () => {
    onChange(index);
    this.checked = true;
  });

  return $input;
}

function Carousel() {
  let $carousel = document.getElementById('carousel');
  const $container = $carousel.querySelector('.carousel_container');
  const $slides = $container.querySelectorAll('figure');
  const $action = $carousel.querySelector('.carousel_action');

  $slides.forEach(($slide, i) => {
    $slide.style.width = $carousel.clientWidth + 'px';
    $action.appendChild(CreateInputRadio(i, i===0, (i) => {
      const width = $carousel.clientWidth;
      $container.style.left = -(width * i) + 'px';
    }));
  });

  window.addEventListener('resize', () => {
    $carousel = document.getElementById('carousel');
    $slides.forEach(($slide, i) => {
      $slide.style.width = $carousel.clientWidth + 'px';
    });
  });

  const lengthSlides = $slides.length;
  const $inputs = $action.querySelectorAll('input[type=radio]');
  let slidePos = 0;
  const interval = setInterval(() => {
    const width = $carousel.clientWidth;

    slidePos++;
    if (slidePos === lengthSlides) slidePos = 0;

    $container.style.left = -(width * slidePos) + 'px';
    $inputs[slidePos].checked = true;
  }, 3500);
}

function main() {
  MainMenu();
  Carousel();
}

document.addEventListener('DOMContentLoaded', main);
