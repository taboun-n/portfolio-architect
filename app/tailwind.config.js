module.exports = {
  purge: [],
  theme: {
    fontFamily: {
      'montserrat': ['Montserrat', 'sans-serif']
    },
    extend: {
      colors: {
        white: '#f7f1e3',
        dark: '#272727',
        action: '#ffb142'
      }
    },
  },
  variants: {},
  plugins: [],
}
